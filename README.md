# Toasty Kitten

A basic placeholder Site for pre-warmed [Kitten](https://codeberg.org/kitten/app) instances created by [Domain](https://codeberg.org/domain/app).

Simply returns a `200 OK` response at `/`.
